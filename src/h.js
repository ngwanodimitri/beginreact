import React from 'react';
import "./app.css";

const periode=['matin','midi','apres-midi','soir']
const jour=['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']
const mois=['Janvier','Fervrier','Mars','Avril','Mais','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre']


class Heure extends React.Component{
  constructor (props){
    super(props)
    this.state={
      horaire: new Date(),
      minutes: new Date().getMinutes(), 
      hours: new Date().getHours(), 
      month: new Date().getMonth(), 
      year: new Date().getFullYear(),
      day: new Date().getDay(),
      quatieme: new Date().getUTCDate(),
      secondes: new Date().getSeconds()
    }
    this.verifDay=null
    this.verifMonth=null
    this.handleDay=this.handleDay.bind(this)
    this.handleMonth=this.handleMonth.bind(this)
  }

  componentDidMount(){
    this.timer=setInterval(this.clock.bind(this), 1000);
  }
  componentWillUnmount(){
    clearInterval(this.timer)
  }

  clock(){
    this.setState({
      minutes: new Date().getMinutes(), 
      hours: new Date().getHours(),  
      year: new Date().getFullYear(),
      secondes: new Date().getSeconds()
    })
  }

  handleDay(e){
    this.verifDay=true
    this.setState({
      day: e.target.value
    })
  }

  handleMonth(e){
    this.verifMonth=true
    this.setState({
      month: e.target.value
    })
  }

  change(){
    var date=new Date(document.getElementById('da').value)
    alert(date.getMonth())
  }
 
  

  render(){
      var PERIODE=1
      const MOIS=this.state.month
      const JOUR=this.state.day
      if(this.state.hours<12 && this.state.hours>=0){
         PERIODE=periode[0]
      }
      else if(this.state.hours===12){
         PERIODE=periode[1]
      }
      else if(this.state.hours>12 && this.state.hours<17){
         PERIODE=periode[2]
      }else{
         PERIODE=periode[3]
      }
      
    return <div>
      
              <div className="horloge">
                {!this.verifDay?
                  <span className="h1 jour">{jour[JOUR - 1]}<br></br></span>:
                  <span className="h1 jour">{this.state.day}<br></br></span>}
                <span className="h1 periode">{PERIODE}</span><br></br>
                <span className="h1 heure">{this.state.hours}:{this.state.minutes}</span><br></br>
                {!this.verifMonth? 
                  <span className="h1 date">{this.state.quatieme}   {mois[MOIS]}  {this.state.year}</span>:
                  <span className="h1 date">{this.state.quatieme}   {this.state.month}  {this.state.year}</span>}
              </div>
              <br></br>
              
              <div className="modifier" id="a">
                <span className="titre">Modifier_Jour: </span>
                <select value={jour[JOUR - 1]} onChange={this.handleDay}>
                  <option value={jour[0]}>lundi</option>
                  <option value={jour[1]}>mardi</option>
                  <option value={jour[2]}>mercredi</option>
                  <option value={jour[3]}>jeudi</option>
                  <option value={jour[4]}>vendredi</option>
                  <option value={jour[5]}>samedi</option>
                  <option value={jour[6]}>dimanche</option>
                </select>

                <span className="titre">Modifier_Mois </span>
                <select value={mois[MOIS]} onChange={this.handleMonth}>
                  <option value={mois[0]}>janvier</option>
                  <option value={mois[1]}>fevrier</option>
                  <option value={mois[2]}>mars</option>
                  <option value={mois[3]}>avril</option>
                  <option value={mois[4]}>mais</option>
                  <option value={mois[5]}>juin</option>
                  <option value={mois[6]}>juillet</option>
                  <option value={mois[7]}>Aout</option>
                  <option value={mois[8]}>septembre</option>
                  <option value={mois[9]}>octobre</option>
                  <option value={mois[10]}>novembre</option>
                  <option value={mois[11]}>decembre</option>
                </select>

                <span className="titre">Modifier_Date </span>
                <select value={jour[JOUR - 1]} onChange={this.handleDay}>
                  <option value={jour[0]}>lundi</option>
                  <option value={jour[1]}>mardi</option>
                  <option value={jour[2]}>mercredi</option>
                  <option value={jour[3]}>jeudi</option>
                  <option value={jour[4]}>vendredi</option>
                  <option value={jour[5]}>samedi</option>
                  <option value={jour[6]}>dimanche</option>
                </select>

                <span className="titre">Modifier_Annee </span>
                <select value={jour[JOUR - 1]} onChange={this.handleDay}>
                  <option value={jour[0]}>lundi</option>
                  <option value={jour[1]}>mardi</option>
                  <option value={jour[2]}>mercredi</option>
                  <option value={jour[3]}>jeudi</option>
                  <option value={jour[4]}>vendredi</option>
                  <option value={jour[5]}>samedi</option>
                  <option value={jour[6]}>dimanche</option>
                </select>
                
              </div>
              <input type="date" onChange={this.change.bind(this)} value="2018-07-22" id="da"></input>
          </div>
  }

}


    




function App() {
  return <div>
  <Heure />
  
  </div>
}

export default App;
