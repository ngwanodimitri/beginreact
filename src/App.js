import React from 'react';
import "./app.css";

const periode=['matin','midi','apres-midi','soir']
const jour=['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']
const mois=['Janvier','Fervrier','Mars','Avril','Mais','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre']


class Heure extends React.Component{
  constructor (props){
    super(props)
    this.state={
      minutes: new Date().getMinutes(), 
      hours: new Date().getHours(), 
      month: new Date().getMonth(), 
      year: new Date().getFullYear(),
      day: new Date().getDay(),
      quatieme: new Date().getUTCDate(),
      secondes: new Date().getSeconds()
    }
    this.statut=null
    this.verifDay=null
    this.verifMonth=null
    this.date=new Date()

  }

  componentDidMount(){
    this.timer=setInterval(this.clock.bind(this), 1000);
  }
  componentWillUnmount(){
    clearInterval(this.timer)
  }

  clock(){
    if(!this.statut){
      this.setState({
        minutes: new Date().getMinutes(), 
        hours: new Date().getHours(), 
        month: new Date().getMonth(), 
        year: new Date().getFullYear(),
        day: new Date().getDay(),
        quatieme: new Date().getUTCDate(),
        secondes: new Date().getSeconds()
      })
    }
    
  }

  change(e){
    this.statut=true
    this.date=new Date(document.getElementById('da').value)
    this.verifDay=true
    this.verifMonth=true
    var j=null
    if((this.date.getDay()-1)=== -1){
      j='DIMANCHE'
    }else{
      j=jour[this.date.getDay()-1]
    }

    
    var m=mois[this.date.getMonth()]
    this.setState({
      day: j,
      month: m,
      year: this.date.getUTCFullYear(),
      quatieme: this.date.getUTCDate()
    })
  }

  change1(e){
    this.statut=true
    var timer=document.getElementById('ti').value
    const heur=parseInt((timer[0]+timer[1]), 10)
    const minut=parseInt((timer[3]+timer[4]), 10)
    this.setState({
      minutes: minut,
      hours: heur
    })
  }
 
  

  render(){
      var PERIODE=1
      const MOIS=this.state.month
      var JOUR=this.state.day
      if(JOUR===0){
        JOUR=7
      }
      if(this.state.hours<12 && this.state.hours>=0){
         PERIODE=periode[0]
      }
      else if(this.state.hours===12){
         PERIODE=periode[1]
      }
      else if(this.state.hours>12 && this.state.hours<17){
         PERIODE=periode[2]
      }else{
         PERIODE=periode[3]
      }
      
    return <div>
      
              <div className="horloge">
                {!this.verifDay?
                  <span className="h1 jour"> {jour[JOUR - 1]}<br></br></span>:
                  <span className="h1 jour">{this.state.day}<br></br></span>}
                <span className="h1 periode">{PERIODE}</span><br></br>
                <span className="h1 heure">{this.state.hours}:{this.state.minutes}</span><br></br>
                {!this.verifMonth? 
                  <span className="h1 date">{this.state.quatieme}   {mois[MOIS]}  {this.state.year}</span>:
                  <span className="h1 date">{this.state.quatieme}   {this.state.month}  {this.state.year}</span>}
              </div>
              <br></br>
              Modifier l'heure:
              <input type ="time" onChange={this.change1.bind(this)} id="ti"></input><br/><br/>
              Modifier la date:
              <input type="date" onChange={this.change.bind(this)}  id="da"></input>
          </div>
  }

}

function App() {
  return <div>
  <Heure />
  
  </div>
}

export default App;
